{
  description = "F# toy scheme";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        # Hack to use project-local fsautocomplete since global installs do
        # not work.
        # See https://github.com/dotnet/sdk/issues/30546
        # and https://github.com/NixOS/nixpkgs/issues/216285
        fsautocomplete = pkgs.writeShellScriptBin "fsautocomplete" ''dotnet fsautocomplete $@'';
        fantomas = pkgs.writeShellScriptBin "fantomas" ''dotnet fantomas $@'';
        in {
          devShell = pkgs.mkShell {
            name = "dotnet-env";
            packages = with pkgs; [
              dotnet-sdk_7
              fsautocomplete
              fantomas
            ];
          };
        });
}
