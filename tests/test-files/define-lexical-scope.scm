(define x 1)

(define (f)
  (define x 5)
  (define (g) x)
  (set! x 3)
  g)

(set! x 10)

((f))
