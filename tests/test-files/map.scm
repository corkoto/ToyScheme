(define (null? xs)
  (equal? xs '()))

(define (map f xs)
  (if (null? xs)
      '()
      (cons (f (car xs)) (map f (cdr xs)))))

(map (lambda (x) (+ x 1)) (list 1 3 5))
