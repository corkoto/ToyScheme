(define x 3)

(define f (let ((x 5)
                (g (lambda () x)))
            g))

(f)
