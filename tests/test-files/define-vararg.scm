(define (f x y . zs)
  (+ x y (apply * zs)))

(f 5 3 1 2 3 4)
