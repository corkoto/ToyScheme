(define (null? xs)
  (equal? xs '()))

(define (fold-left op acc xs)
  (if (null? xs)
      acc
      (fold-left op (op acc (car xs)) (cdr xs))))

(define (flipcons xs x) (cons x xs))

(define (rev xs)
  (fold-left flipcons '() xs))

(rev '(1 2 3))
