(define (incn n)
  (lambda (x) (+ x n)))

((incn 3) 5)
