module Tests

open NUnit.Framework
open Types
open Eval
open Primitives
open Parsers
open System.IO

let evalFile (env: EvalEnv) (f: string) : Result<LispVal, ErrorTrace> =
    match runParseFile f with
    | Ok exprs -> evalSequentially env exprs |> Result.map List.last
    | Error msg -> failwith msg

let joinTestDir (file: string) : string = Path.Join("test-files", file)

// Since LispVal contains functions custom equality is required if we we want to
// use the type directly in testing. For simplicity we opt for the easier
// solution of string-based comparisons by printing the resulting value.
let testEvalFile (file: string) (expected: string) : Unit =
    let env = createPrimitiveBindings ()

    match joinTestDir file |> evalFile env with
    | Result.Ok res -> Assert.That(showVal res, Is.EqualTo(expected))
    | Result.Error e -> e |> showErrorTrace |> Assert.Fail

[<SetUp>]
let Setup () = ()

[<Test>]
let Map () = testEvalFile "map.scm" "(2 4 6)"

[<Test>]
let LexicalScopingWithDefine () =
    testEvalFile "define-lexical-scope.scm" "3"

[<Test>]
let Factorial () = testEvalFile "factorial.scm" "3628800"

[<Test>]
let FuncallExpression () =
    testEvalFile "funcall-expression.scm" "8"

[<Test>]
let ShadowingLambda () =
    testEvalFile "shadowing-lambda.scm" "12"

[<Test>]
let MutualRecursion () =
    testEvalFile "mutual-recursion.scm" "#t"

[<Test>]
let LeftFold () = testEvalFile "left-fold.scm" "(3 2 1)"

[<Test>]
let DefineVararg () = testEvalFile "define-vararg.scm" "32"

[<Test>]
let LambdaVararg () = testEvalFile "lambda-vararg.scm" "24"

[<Test>]
let LetLambdaScoping () =
    testEvalFile "let-lambda-scoping.scm" "3"

[<Test>]
let LetStarLambdaScoping () =
    testEvalFile "let-star-lambda-scoping.scm" "5"

[<Test>]
let LetrecMutualRecursion () =
    testEvalFile "letrec-mutual-recursion.scm" "#f"

[<Test>]
let ListEquality () = testEvalFile "list-equality.scm" "#t"

[<Test>]
let OrNoArgs () = testEvalFile "or-no-args.scm" "#f"

[<Test>]
let OrReturnsFirstTruthy () = testEvalFile "or-first-truthy.scm" "0"

[<Test>]
let AndNoArgs () = testEvalFile "and-no-args.scm" "#t"

[<Test>]
let AndReturnsLastTruthy () = testEvalFile "and-last-truthy.scm" "0"

[<Test>]
let Addition () = testEvalFile "addition.scm" "9"

[<Test>]
let AdditionNoArgs () = testEvalFile "addition-no-args.scm" "0"

[<Test>]
let Multiplication () =
    testEvalFile "multiplication.scm" "-150"

[<Test>]
let MultiplicationNoArgs () =
    testEvalFile "multiplication-no-args.scm" "1"

[<Test>]
let Subtraction () = testEvalFile "subtraction.scm" "4"

[<Test>]
let SubtractionSingleArg () =
    testEvalFile "subtraction-single-arg.scm" "-5"

[<Test>]
let Division () = testEvalFile "division.scm" "-8"
