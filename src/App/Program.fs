module Program

open Parsers
open Types
open Primitives
open Eval
open System
open FsToolkit.ErrorHandling

let readPrompt (prompt: string) : Option<string> =
    printf "%s" prompt

    match Console.ReadLine() with
    | null -> None
    | s -> s + "\n" |> Some

let rec repl (env: EvalEnv) (currentInput: string list) : Unit =
    // Could probably use difference lists from FSharpx to avoid reversing the
    // list here, but that's probably redundant since REPL inputs are rarely
    // long enough to impact performance significantly.
    let formatInput = List.rev >> String.concat ""

    match readPrompt (if currentInput.IsEmpty then "> " else "") with
    | None -> printf "\n"
    | Some s ->
        let newInput = s :: currentInput

        match newInput |> formatInput |> runReadReplExpr with
        | Error IncompleteInput -> repl env newInput
        | Error(ParseError msg) ->
            printfn "%s" msg
            repl env []
        | Ok expr ->
            match eval env expr with
            | Ok expr ->
                expr |> showVal |> printfn "%s"
                repl env []
            | Error err ->
                printfn "%s" (showErrorTrace err)
                repl env []

[<EntryPoint>]
let main (argv: string[]) : int =
    let env = createPrimitiveBindings ()

    match argv with
    | [||] ->
        repl env []
        0
    | [| file |] ->
        result {
            let! exprs = runParseFile file

            let! values =
                evalSequentially env exprs |> Result.mapError showErrorTrace

            return List.last values
        }
        |> function
            | Ok value ->
                value |> showVal |> printfn "%s"
                0
            | Error msg ->
                printfn "%s" msg
                1
    | _ ->
        printfn "%s" "Expected exactly 0 or 1 arguments"
        1
