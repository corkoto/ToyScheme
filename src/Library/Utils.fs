module Utils

let uncurry f (a, b) = f a b

let unwords (ws: string list) =
    let rec unwords' ws acc =
        match ws with
        | [] -> acc
        | [ w ] -> acc + w
        | w :: ws' -> unwords' ws' (acc + w + " ") in

    unwords' ws ""

let skipLast (xs: 'a list) : 'a list =
    let rec skipLast' (st: 'a list) (xs: 'a list) =
        match (st, xs) with
        | (st, []) -> st
        | (st, [ _ ]) -> st
        | (st, x :: xs) -> skipLast' (x :: st) xs

    skipLast' [] xs
