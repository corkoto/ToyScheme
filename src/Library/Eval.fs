module Eval

open System.IO
open FsToolkit.ErrorHandling
open Types
open Primitives
open Utils
open System

let define
    (env: EvalEnv)
    (ident: string)
    (value: LispVal)
    : Result<Unit, ErrorTrace> =
    if env.DefinedInCurrentScope ident then
        ErrorTrace(env.StackTrace, MultipleDefineInSameScope ident) |> Error
    else
        let heapRef = env.AddToHeap value
        (ident, heapRef) |> env.AddBinding |> Ok

// Assign a new value to the most recent declaration of the provided
// identifier.
let set
    (env: EvalEnv)
    (ident: string)
    (value: LispVal)
    : Result<Unit, ErrorTrace> =
    if env.Set(ident, value) then
        Ok()
    else
        ErrorTrace(env.StackTrace, SettingUndefinedIdentifier ident) |> Error

let variableUses (body: LispVal list) : string list =
    let rec variableUses' (value: LispVal) : string list =
        match value with
        | LispSym s -> [ s ]
        | LispList xs -> List.map variableUses' xs |> List.concat
        | DottedLispList(xs, x) ->
            List.map variableUses' xs
            |> List.concat
            |> List.append (variableUses' x)
        | LispNum _ -> []
        | LispString _ -> []
        | LispBool _ -> []
        | PrimitiveLispFunc _ -> []
        | LispFunc _ -> []
        | Unbound -> []

    body |> List.map variableUses' |> List.concat

let makeFunc
    (displayName: Option<string>)
    (vararg: string option)
    (env: EvalEnv)
    (parameters: LispVal list)
    (body: LispVal list)
    : Result<LispVal, ErrorTrace> =
    result {
        if List.isEmpty body then
            do!
                ErrorTrace(
                    env.StackTrace,
                    "Empty definitions are not allowed" |> BadForm
                )
                |> Error

        let getSymbol =
            function
            | LispSym n -> Ok n
            | _ ->
                ErrorTrace(
                    env.StackTrace,
                    BadForm "Parameter list must consist of symbols"
                )
                |> Error

        let! paramNames = List.traverseResultM getSymbol parameters

        let boundVars =
            match vararg with
            | Some rest -> paramNames @ [ rest ]
            | None -> paramNames

        let freeVars = List.except boundVars (variableUses body)

        let closure =
            freeVars
            |> List.choose (fun ident ->
                env.LookupRef ident |> Option.map (fun r -> (ident, r)))
            |> Map.ofList

        return
            LispFunc
                { parameters = paramNames
                  vararg = vararg
                  body = body
                  closure = closure
                  displayName = displayName }
    }

let makeNormalFunc displayName = makeFunc displayName None
let makeVarArgs displayName = Some >> makeFunc displayName

let checkLetBindings
    (callerName: string)
    (env: EvalEnv)
    (bindings: LispVal list)
    : Result<(string * LispVal) list, ErrorTrace> =
    List.traverseResultM
        (fun b ->
            match b with
            | LispList [ LispSym ident; form ] -> Ok(ident, form)
            | _ ->
                ErrorTrace(
                    env.StackTrace,
                    sprintf
                        "%s: expected bindings to consist of pairs in the form (<identifier> <form>)"
                        callerName
                    |> BadForm
                )
                |> Error)
        bindings

let rec eval (env: EvalEnv) (value: LispVal) : Result<LispVal, ErrorTrace> =
    match value with
    | LispString _
    | LispNum _
    | LispBool _
    | LispFunc _
    | PrimitiveLispFunc _ -> Ok value
    | LispSym ident ->
        ident
        |> env.LookupValue
        |> function
            | Some v -> v |> Ok
            | None ->
                ErrorTrace(env.StackTrace, UnboundIdentifier ident) |> Error
    | LispList(LispSym "quote" :: args) ->
        match args with
        | [ expr ] -> Ok expr
        | _ ->
            ErrorTrace(
                env.StackTrace,
                BadForm "quote: expected single argument"
            )
            |> Error
    | LispList(LispSym "if" :: args) ->
        match args with
        | [ pred; expr1; expr2 ] ->
            result {
                let! res = eval env pred
                return! if lispTrue res then eval env expr1 else eval env expr2
            }
        | _ ->
            ErrorTrace(
                env.StackTrace,
                InvalidAmountOfArguments("if", List.length args)
            )
            |> Error
    // Debug function
    | LispList(LispSym "dump-env" :: args) ->
        match args with
        | [] ->
            let contents = env.Show(showVal)
            File.WriteAllText("env-dump", contents)
            Ok nil
        | _ ->
            ErrorTrace(
                env.StackTrace,
                InvalidAmountOfArguments("dump-env", List.length args)
            )
            |> Error
    | LispList(LispSym "collect-garbage" :: args) ->
        match args with
        | [] ->
            env.CollectGarbage()
            Ok nil
        | _ ->
            ErrorTrace(
                env.StackTrace,
                InvalidAmountOfArguments("dump-env", List.length args)
            )
            |> Error
    | LispList(LispSym "print" :: args) ->
        match args with
        | [ expr ] ->
            showVal expr |> printfn "%s"
            Ok nil
        | _ ->
            ErrorTrace(
                env.StackTrace,
                InvalidAmountOfArguments("print", List.length args)
            )
            |> Error
    // Variable assignment
    | LispList(LispSym "set!" :: args) ->
        match args with
        | [ LispSym var; form ] ->
            result {
                let! res = eval env form
                do! set env var res
                return nil
            }
        | _ ->
            ErrorTrace(
                env.StackTrace,
                BadForm
                    "set!: should be used with a symbol and an expression to assign to it"
            )
            |> Error
    | LispList(LispSym "let" :: args) ->
        match args with
        | LispList bindings :: body ->
            let evalBinding k v =
                result {
                    let! res = eval env v
                    return (k, res)
                }

            result {
                let! checkedBindings = checkLetBindings "let" env bindings
                env.Push("let", false, [])

                let! evaluatedBindings =
                    List.traverseResultM
                        (fun (k, v) -> evalBinding k v)
                        checkedBindings

                List.traverseResultM (uncurry (define env)) evaluatedBindings
                |> ignore

                let! res = evalSequentially env body
                env.Pop(1)
                return List.last res
            }
        | _ ->
            ErrorTrace(
                env.StackTrace,
                BadForm
                    "let: should be called with a list of binding pairs and a body"
            )
            |> Error
    | LispList(LispSym "let*" :: args) ->
        match args with
        | LispList bindings :: body ->
            let evalBinding k v =
                result {
                    let! res = eval env v
                    do! define env k res
                }

            result {
                let! checkedBindings = checkLetBindings "let*" env bindings
                env.Push("let*", false, [])

                List.traverseResultM
                    (fun (k, v) -> evalBinding k v)
                    checkedBindings
                |> ignore

                let! res = evalSequentially env body
                env.Pop(1)
                return List.last res
            }
        | _ ->
            ErrorTrace(
                env.StackTrace,
                BadForm
                    "let*: should be called with a list of binding pairs and a body"
            )
            |> Error
    | LispList(LispSym "letrec" :: args) ->
        match args with
        | LispList bindings :: body ->
            let evalBinding k v =
                result {
                    let! res = eval env v
                    return (k, res)
                }

            result {
                let! checkedBindings = checkLetBindings "letrec" env bindings
                env.Push("letrec", false, [])
                List.traverseResultM (fun k -> define env k Unbound) |> ignore

                let! evaluatedBindings =
                    List.traverseResultM
                        (fun (k, v) -> evalBinding k v)
                        checkedBindings

                List.traverseResultM (uncurry (define env)) evaluatedBindings
                |> ignore

                let! res = evalSequentially env body
                env.Pop(1)
                return List.last res
            }
        | _ ->
            ErrorTrace(
                env.StackTrace,
                BadForm
                    "letrec: should be called with a list of binding pairs and a body"
            )
            |> Error
    // Variable definition
    | LispList(LispSym "define" :: args) ->
        match args with
        | [ LispSym ident; form ] ->
            result {
                do! define env ident Unbound
                let! res = eval env form
                do! set env ident res
                return nil
            }
        // Normal function definition
        | LispList(LispSym ident :: args) :: body ->
            result {
                // Pre-allocate an unbound dummy identifier to allow for self
                // recursion.
                do! define env ident Unbound
                let! func = makeNormalFunc (Some ident) env args body
                do! set env ident func
                return nil
            }
        // Function definition with variadic arguments
        | DottedLispList(LispSym ident :: args, LispSym vararg) :: body ->
            result {
                let! func = makeVarArgs (Some ident) vararg env args body
                do! define env ident func
                return nil
            }
        | _ ->
            ErrorTrace(env.StackTrace, BadForm "define: invalid form") |> Error
    | LispList(LispSym "lambda" :: args) ->
        match args with
        | LispList args :: body -> makeNormalFunc None env args body
        | DottedLispList(args, LispSym vararg) :: body ->
            makeVarArgs None vararg env args body
        // Form where the only parameter is variadic
        | LispSym vararg :: body -> makeVarArgs None vararg env [] body
        | _ ->
            ErrorTrace(env.StackTrace, BadForm "lambda: invalid form") |> Error
    | LispList(LispSym "apply" :: exprs) ->
        result {
            let! res = evalSequentially env exprs

            return!
                match res with
                | [ f; LispList arg ] -> apply env (f :: arg)
                | _ ->
                    ErrorTrace(env.StackTrace, BadForm "apply: invalid form")
                    |> Error
        }
    | LispList exprs ->
        result {
            let! res = evalSequentially env exprs
            return! apply env res
        }
    | Unbound -> raise (InvalidProgramException "attempted to evaluate Unbound")
    | DottedLispList(_, _) ->
        ErrorTrace(env.StackTrace, BadForm "Use of unquoted dotted list")
        |> Error

and evalSequentially
    (env: EvalEnv)
    : LispVal list -> Result<LispVal list, ErrorTrace> =
    List.traverseResultM (eval env)

and apply (env: EvalEnv) (exprs: LispVal list) : Result<LispVal, ErrorTrace> =
    match exprs with
    | PrimitiveLispFunc(n, f) :: args ->
        env.Push(n, false, [])
        let res = f env args
        env.Pop(1)
        res
    | LispFunc f :: args -> applyNonPrimitive env f args
    | x :: _ -> ErrorTrace(env.StackTrace, CallOfNonFunction x) |> Error
    | [] ->
        ErrorTrace(env.StackTrace, BadForm "Use of unquoted empty list")
        |> Error

and applyNonPrimitive
    (env: EvalEnv)
    (f: LispFunc)
    (args: list<LispVal>)
    : Result<LispVal, ErrorTrace> =
    result {
        let scopeName =
            match f.displayName with
            | Some x -> x
            | None -> showVal (LispFunc f)

        // Use separate scopes for the closure and locally defined values. This
        // is to ensure that variables defined in the function will shadow the
        // ones defined in the closure.
        env.Push($"{scopeName} closure", true, Map.toSeq f.closure)
        env.Push(scopeName, false, [])

        if List.length f.parameters <> List.length args && f.vararg = None then
            do!
                ErrorTrace(
                    env.StackTrace,
                    InvalidAmountOfArguments(scopeName, List.length args)
                )
                |> Error

        let restArgs = List.skip (List.length f.parameters) args

        // Evaluate the body in a new environment where the
        // parameters are substituted for the arguments. Lazy zip
        // used to truncate the longer list which is necessary for
        // varargs.
        let paramBindings: (string * LispVal) list =
            Seq.zip f.parameters args |> List.ofSeq

        List.traverseResultM (uncurry (define env)) paramBindings |> ignore

        match f.vararg with
        | None -> ()
        | Some n -> LispList restArgs |> define env n |> ignore

        let! res = List.traverseResultM (eval env) f.body
        // Pop both the closure and local environement.
        env.Pop(2)
        return List.last res
    }
