module Types

open System.Collections.Generic
open Utils

type HeapRef = int

type StackTrace = string list

type Scope =
    { label: string
      isClosure: bool
      bindings: Dictionary<string, HeapRef> }

let showBindings
    (formatter: string -> HeapRef -> string)
    (sep: string)
    (bindings: seq<(KeyValuePair<string, HeapRef>)>)
    : string =
    Seq.map (fun (KeyValue(k, v)) -> formatter k v) bindings
    |> String.concat sep

type LispError =
    | MultipleDefineInSameScope of string
    | UnboundIdentifier of string
    | SettingUndefinedIdentifier of string
    | InvalidAmountOfArguments of string * int
    | CallOfNonFunction of LispVal
    | BadForm of string
    | DivisionByZero

and ErrorTrace = ErrorTrace of StackTrace * LispError

and LispVal =
    | LispSym of string
    | LispList of list<LispVal>
    | DottedLispList of list<LispVal> * LispVal
    | LispNum of int
    | LispString of string
    | LispBool of bool
    | PrimitiveLispFunc of
        (string * (EvalEnv -> list<LispVal> -> Result<LispVal, ErrorTrace>))
    | LispFunc of LispFunc
    // Only used as a temporary value to allow recursion; should never occur in
    // evaluation
    | Unbound

and LispFunc =
    { parameters: list<string>
      vararg: Option<string>
      body: list<LispVal>
      closure: Map<string, HeapRef>
      displayName: Option<string> }


and EvalEnv(bindings: seq<(string * LispVal)>) =
    let mutable gcThreshold = 10000
    let gcHighSurvivalRatePercentage = 70

    let stack = Stack<Scope>()

    let mutable heap = Dictionary<HeapRef, LispVal>()

    let show (valueFormatter: LispVal -> string) : string =
        let stackStr =
            stack
            |> Seq.map (fun x ->
                sprintf
                    "%s:\n%s"
                    x.label
                    (showBindings
                        (fun k v -> sprintf "%A: %A" k v)
                        "\n"
                        x.bindings))
            |> String.concat "\n"

        let heapStr =
            heap
            |> List.ofSeq
            |> List.sortBy (fun x -> x.Key)
            |> List.map (fun (KeyValue(k, v)) ->
                sprintf "%A: %s" k (valueFormatter v))
            |> String.concat "\n"

        "-- Stack --\n" + stackStr + "\n" + "-- Heap --\n" + heapStr + "\n"

    let rec collectGarbage: Unit -> Unit =
        fun () ->
            let gcRoots: HeapRef list =
                stack
                |> Seq.map (fun scope -> scope.bindings.Values)
                |> Seq.concat
                |> List.ofSeq

            let reachableFromClosures: HeapRef Set =

                let rec traverseGcChain
                    (traversed: HeapRef list)
                    (r: HeapRef)
                    (v: LispVal)
                    : HeapRef list =
                    if
                        List.contains r traversed // Return immediately on circular references.
                    then
                        []
                    else
                        match v with
                        | LispFunc f ->
                            f.closure
                            |> Map.values
                            |> Seq.map (fun r' ->
                                let v' = heap[r'] in
                                traverseGcChain (r :: traversed) r' v')
                            |> Seq.concat
                            |> List.ofSeq
                        | _ -> [ r ]

                heap
                |> Seq.filter (fun (KeyValue(r, _)) -> List.contains r gcRoots)
                |> Seq.map (fun x -> traverseGcChain [] x.Key x.Value)
                |> Seq.concat
                |> Set.ofSeq

            let heapKeys = heap.Keys |> Set.ofSeq

            let deletionCandidates =
                Set.difference
                    heapKeys
                    (Set.union (Set.ofList gcRoots) reachableFromClosures)

            Set.iter
                (fun r ->
                    if Set.contains r deletionCandidates then
                        heap.Remove(r) |> ignore)
                heapKeys
            // Create a new heap from the values in the old heap without any gaps
            // between the keys in it that might have resulted from removal.  Also
            // modify the references in the stack as well as the closures in the
            // heap to account for this.
            let refReplaceMap: Map<HeapRef, HeapRef> =
                heap |> Seq.mapi (fun i (KeyValue(r, _)) -> (r, i)) |> Map

            Seq.iter
                (fun scope ->
                    Seq.iter
                        (fun (KeyValue(k, r)) ->
                            scope.bindings[k] <- refReplaceMap[r])
                        scope.bindings)
                stack

            Seq.iter
                (fun (KeyValue(k, v)) ->
                    match v with
                    | LispFunc f ->
                        let newClosure =
                            Map.map (fun _ r -> refReplaceMap[r]) f.closure in

                        heap[k] <- LispFunc { f with closure = newClosure }
                    | _ -> ())
                heap

            let newHeap =
                Seq.map (fun (KeyValue(k, v)) -> (refReplaceMap[k], v)) heap
                |> dict
                |> Dictionary

            heap <- newHeap
            // Increase the GC threshold if the object survival rate is still high
            // after garbage collection.
            if
                heap.Count > gcThreshold * 100 * gcHighSurvivalRatePercentage
                             / 100
            then
                gcThreshold <- gcThreshold * 2


    let getFromHeap (r: HeapRef) : LispVal =
        let (exists, item) = heap.TryGetValue r

        if exists then
            item
        else
            raise (KeyNotFoundException $"No heap item with reference {r}")

    let addToHeap (v: LispVal) : HeapRef =
        if heap.Count > gcThreshold then
            collectGarbage ()

        let heapRef = heap.Count
        heap.Add(heapRef, v)
        heapRef

    // Look up the most recent binding associated with the identifier.
    let lookupBinding (ident: string) : Option<HeapRef> =
        stack
        |> Seq.map (fun scope -> scope.bindings.TryGetValue ident)
        |> Seq.tryFind (fun (success, _) -> success)
        |> Option.map snd

    let push
        (
            label: string,
            isClosure: bool,
            bindings: seq<(string * LispVal)>
        ) : Unit =
        let bs =
            Seq.map (fun (n, v) -> (n, addToHeap v)) bindings
            |> dict
            |> Dictionary

        { label = label
          isClosure = isClosure
          bindings = bs }
        |> stack.Push

    do push ("TOP LEVEL", false, bindings)

    member _.AddToHeap: LispVal -> HeapRef = addToHeap

    member _.AddBinding(n: string, r: HeapRef) : Unit =
        stack.Peek().bindings.Add(n, r)

    // Push a new scope of bindings onto the stack.
    member _.Push
        (
            label: string,
            isClosure: bool,
            bindings: seq<(string * HeapRef)>
        ) =
        let bs = bindings |> dict |> Dictionary

        { label = label
          isClosure = isClosure
          bindings = bs }
        |> stack.Push

    // Pop n entries in the stack.
    member _.Pop(n: int) : Unit =
        for _ in 1..n do
            stack.Pop() |> ignore

    // Look up the most recent binding associated with the identifier.
    member _.LookupRef: string -> Option<HeapRef> = lookupBinding

    // Look up the value associated with the reference of the most recent
    // binding associated with the identifier.
    member _.LookupValue: string -> Option<LispVal> =
        lookupBinding >> Option.map getFromHeap

    // Assign a new value to the most recent declaration of the provided
    // identifier.
    member _.Set(ident: string, value: LispVal) : bool =
        match
            stack |> Seq.tryFind (fun scope -> scope.bindings.ContainsKey ident)
        with
        | Some scope ->
            let r = scope.bindings.[ident]
            heap[r] <- value
            true
        | None -> false

    // Check if the topmost scope in the stack contains the given identifier.
    member _.DefinedInCurrentScope(ident: string) : bool =
        stack.Peek().bindings.ContainsKey(ident)

    member _.StackTrace: StackTrace =
        stack
        |> Seq.where (fun s -> not s.isClosure)
        |> Seq.map (fun s -> s.label)
        |> Seq.toList
        |> skipLast

    member _.CollectGarbage() : Unit = collectGarbage ()

    // Pretty print the current state of the stack and heap.
    member _.Show(valueFormatter) : string = show valueFormatter

let rec showVal (value: LispVal) : string =
    let listWrap (s: string) = "(" + s + ")"

    match value with
    | LispString s -> "\"" + s + "\""
    | LispSym a -> a
    | LispNum x -> string x
    | LispBool true -> "#t"
    | LispBool false -> "#f"
    | LispList xs -> List.map showVal xs |> unwords |> listWrap
    | DottedLispList(init, last) ->
        unwords (List.map showVal init) + " . " + showVal last |> listWrap
    | (PrimitiveLispFunc _) -> "<primitive>"
    | LispFunc { parameters = args
                 vararg = rest
                 body = body
                 closure = clos
                 displayName = displayName } ->
        let name =
            match displayName with
            | Some x -> x
            | None -> "lambda"

        let varargString =
            match rest with
            | Some arg -> " . " + arg
            | None -> ""

        sprintf
            "(closure (%s) (%s (%s%s) %s))"
            (showBindings (fun k v -> sprintf "(%A . %A)" k v) " " clos)
            name
            (unwords args)
            varargString
            (showVal (LispList body))
    | Unbound -> "<unbound>"


let showError: LispError -> string =
    function
    | MultipleDefineInSameScope s -> sprintf "redefinition of identifier %s" s
    | UnboundIdentifier s -> sprintf "unbound identifier: %s" s
    | SettingUndefinedIdentifier s ->
        sprintf "attempt to assign value to undefined identifier: %s" s
    | InvalidAmountOfArguments(s, n) ->
        sprintf "invalid amount of arguments to %s (%A provided)" s n
    | CallOfNonFunction v ->
        sprintf "attempted to call non-function: %s" (showVal v)
    | BadForm s -> sprintf "bad form: %s" s
    | DivisionByZero -> "attempted division by zero"

let showErrorTrace (ErrorTrace(t, e)) : string =
    let prettyTrace = String.concat ", called from:\n" t
    let prettyErr = showError e

    match t with
    | [] -> sprintf "Error: %s" prettyErr
    | _ -> sprintf "Error: %s in:\n%s" prettyErr prettyTrace
