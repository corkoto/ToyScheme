module Primitives

open FsToolkit.ErrorHandling
open Types

let nil = LispList []

let getNum (env: EvalEnv) (v: LispVal) : Result<int, ErrorTrace> =
    match v with
    | LispNum x -> Ok x
    | _ ->
        ErrorTrace(env.StackTrace, BadForm("Not a number: " + showVal v))
        |> Error

let getNums: EvalEnv -> LispVal list -> Result<int list, ErrorTrace> =
    getNum >> List.traverseResultM

let lispSum (env: EvalEnv) (args: LispVal list) : Result<LispVal, ErrorTrace> =
    getNums env args |> Result.map (List.fold (+) 0 >> LispNum)

let lispProduct
    (env: EvalEnv)
    (args: LispVal list)
    : Result<LispVal, ErrorTrace> =
    getNums env args |> Result.map (List.fold (*) 1 >> LispNum)

let lispDifference
    (env: EvalEnv)
    (args: LispVal list)
    : Result<LispVal, ErrorTrace> =
    result {
        let! product =
            match args with
            | [] ->
                ErrorTrace(env.StackTrace, InvalidAmountOfArguments("-", 0))
                |> Error
            | [ x ] -> x |> getNum env |> Result.map (fun x' -> -x' |> LispNum)
            | _ ->
                args |> getNums env |> Result.map (List.reduce (-) >> LispNum)

        return product
    }

let lispDivision
    (env: EvalEnv)
    (args: LispVal list)
    : Result<LispVal, ErrorTrace> =
    result {
        let! quotient =
            match args with
            | []
            | [ _ ] ->
                ErrorTrace(
                    env.StackTrace,
                    InvalidAmountOfArguments("/", List.length args)
                )
                |> Error
            | _ ->
                result {
                    let! xs' = getNums env args

                    if List.exists ((=) 0) (List.tail xs') then
                        do! ErrorTrace(env.StackTrace, DivisionByZero) |> Error

                    return (List.reduce (/) >> LispNum) xs'
                }

        return quotient
    }

let numBoolBinOp
    (op: int -> int -> bool)
    (opName: string)
    (env: EvalEnv)
    (args: list<LispVal>)
    : Result<LispVal, ErrorTrace> =
    match args with
    | [ x; y ] ->
        result {
            let! x' = getNum env x
            let! y' = getNum env y
            return op x' y' |> LispBool
        }
    | _ ->
        ErrorTrace(
            env.StackTrace,
            InvalidAmountOfArguments(opName, List.length args)
        )
        |> Error

let lispTrue (x: LispVal) : bool =
    match x with
    | LispBool false -> false
    | _ -> true

// Evaluate args and return the first one that is true.
let lispOr (_: EvalEnv) (args: LispVal list) : Result<LispVal, ErrorTrace> =
    let folder (x: LispVal) (y: LispVal) = if lispTrue x then x else y

    List.fold folder (LispBool false) args |> Ok

// Evaluate args until one is false.
// If all of them are true, return the last one.
let lispAnd (_: EvalEnv) (args: LispVal list) : Result<LispVal, ErrorTrace> =
    let folder (x: LispVal) (y: LispVal) =
        if lispTrue x then y else LispBool false

    List.fold folder (LispBool true) args |> Ok

let lispNot (env: EvalEnv) (args: list<LispVal>) : Result<LispVal, ErrorTrace> =
    match args with
    | [ x ] -> Ok <| if lispTrue x then LispBool false else LispBool true
    | _ ->
        ErrorTrace(
            env.StackTrace,
            InvalidAmountOfArguments("not", List.length args)
        )
        |> Error

let lispCar (env: EvalEnv) (args: list<LispVal>) : Result<LispVal, ErrorTrace> =
    match args with
    | [ x ] ->
        match x with
        | LispList(x :: _) -> Ok x
        | LispList [] ->
            ErrorTrace(env.StackTrace, BadForm "car: Empty list") |> Error
        | DottedLispList(x :: _, _) -> Ok x
        | DottedLispList([], _) ->
            ErrorTrace(env.StackTrace, BadForm "car: Empty list") |> Error
        | _ -> ErrorTrace(env.StackTrace, BadForm "car: not a list") |> Error
    | _ ->
        ErrorTrace(
            env.StackTrace,
            InvalidAmountOfArguments("car", List.length args)
        )
        |> Error

let lispCdr (env: EvalEnv) (args: list<LispVal>) : Result<LispVal, ErrorTrace> =
    match args with
    | [ x ] ->
        match x with
        | LispList(_ :: xs) -> LispList xs |> Ok
        | LispList [] ->
            ErrorTrace(env.StackTrace, BadForm "cdr: Empty list") |> Error
        | DottedLispList(_ :: xs, _) -> LispList xs |> Ok
        | DottedLispList([], _) ->
            ErrorTrace(env.StackTrace, BadForm "cdr: Empty list") |> Error
        | _ -> ErrorTrace(env.StackTrace, BadForm "cdr: not a list") |> Error
    | _ ->
        ErrorTrace(
            env.StackTrace,
            InvalidAmountOfArguments("cdr", List.length args)
        )
        |> Error

let lispCons
    (env: EvalEnv)
    (args: list<LispVal>)
    : Result<LispVal, ErrorTrace> =
    match args with
    | [ x; LispList xs ] -> LispList(x :: xs) |> Ok
    | [ x; DottedLispList(xs, final) ] -> DottedLispList(x :: xs, final) |> Ok
    | [ expr1; expr2 ] -> DottedLispList([ expr1 ], expr2) |> Ok
    | [ _ ] -> ErrorTrace(env.StackTrace, BadForm "cons: invalid args") |> Error
    | []
    | _ :: _ ->
        ErrorTrace(
            env.StackTrace,
            InvalidAmountOfArguments("cons", List.length args)
        )
        |> Error

let lispList (_: EvalEnv) (args: list<LispVal>) : Result<LispVal, ErrorTrace> =
    args |> LispList |> Ok

let lispEqual
    (env: EvalEnv)
    (args: LispVal list)
    : Result<LispVal, ErrorTrace> =
    match args with
    | [ v1; v2 ] ->
        let rec eq x y =
            match (x, y) with
            | LispSym s1, LispSym s2 -> s1 = s2
            | LispNum x, LispNum y -> x = y
            | LispBool x, LispBool y -> x = y
            | LispString s1, LispString s2 -> s1 = s2
            | LispList xs, LispList ys -> listEq xs ys
            | DottedLispList(xs, x'), DottedLispList(ys, y') ->
                listEq xs ys && eq x' y'
            | _, _ -> false

        and listEq xs ys =
            List.length xs = List.length ys && List.forall2 eq xs ys

        eq v1 v2 |> LispBool |> Ok
    | _ ->
        ErrorTrace(
            env.StackTrace,
            InvalidAmountOfArguments("equal?", List.length args)
        )
        |> Error

let primitives
    : list<(string * (EvalEnv -> LispVal list -> Result<LispVal, ErrorTrace>))> =
    [ ("+", lispSum)
      ("-", lispDifference)
      ("*", lispProduct)
      ("/", lispDivision)
      ("=", numBoolBinOp (=) "=")
      ("/=", numBoolBinOp (<>) "/=")
      (">", numBoolBinOp (>) ">")
      (">=", numBoolBinOp (>=) ">=")
      ("<", numBoolBinOp (<) "<")
      ("<=", numBoolBinOp (<=) "<=")
      ("or", lispOr)
      ("and", lispAnd)
      ("not", lispNot)
      ("car", lispCar)
      ("cdr", lispCdr)
      ("cons", lispCons)
      ("list", lispList)
      ("equal?", lispEqual) ]

let createPrimitiveBindings: Unit -> EvalEnv =
    fun () ->
        let bindings =
            List.map (fun (n, f) -> (n, PrimitiveLispFunc(n, f))) primitives

        EvalEnv(bindings)
