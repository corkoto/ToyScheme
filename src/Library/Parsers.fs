module Parsers

open FParsec.Primitives
open FParsec.CharParsers
open Types

let symbol: Parser<char, unit> = anyOf "!#$%&|*+-/:<=>?@^_~"

let comment: Parser<unit, unit> = pchar ';' >>. skipRestOfLine false

let insignificant: Parser<unit, unit> =
    skipMany1 (comment <|> spaces1) <|> spaces

let lexeme (p: Parser<'a, unit>) : Parser<'a, unit> = p .>> insignificant

let parseString: Parser<LispVal, unit> =
    parse {
        let! s = (skipChar '"') >>. manyCharsTill anyChar (skipChar '"')
        return LispString s
    }
    |> lexeme

let parseSymbol: Parser<LispVal, unit> =
    parse {
        // The first character may not be a digit.
        let! first = letter <|> symbol
        let! rest = manyChars (letter <|> digit <|> symbol)
        let sym = string first + rest

        return
            match sym with
            | "#t" -> LispBool true
            | "#f" -> LispBool false
            | _ -> LispSym sym
    }
    |> lexeme

let parseNumber: Parser<LispVal, unit> =
    parse {
        let! x = pint32
        return LispNum x
    }
    |> lexeme

// Forward declaration of the main parser function; necessary due to
// mutual recursion.
let (parseExpr: Parser<LispVal, unit>), (parseExpr': Parser<LispVal, unit> ref) =
    createParserForwardedToRef ()

let parseNormalList: Parser<LispVal, unit> =
    lexeme (pchar '(') >>. many parseExpr .>> lexeme (pchar ')') |>> LispList

let parseDottedList: Parser<LispVal, unit> =
    parse {
        do! lexeme (skipChar '(')
        let! init = many parseExpr
        let! last = lexeme (pchar '.') >>. parseExpr
        do! lexeme (skipChar ')')
        return DottedLispList(init, last)
    }

let parseList: Parser<LispVal, unit> =
    attempt parseNormalList <|> parseDottedList

let parseQuoted: Parser<LispVal, unit> =
    parse {
        do! skipChar '\''
        let! x = parseExpr
        return LispList [ LispSym "quote"; x ]
    }

parseExpr'.Value <-
    parseNumber <|> parseSymbol <|> parseString <|> parseQuoted <|> parseList

// For user interaction: ignore leading and trailing whitespace, but
// ensure there are no other trailing characters as well.
let parseInteractive: Parser<LispVal, unit> =
    insignificant >>. parseExpr .>> eof

// Useful for parsing files.
let parseManyExprs: Parser<LispVal list, unit> =
    insignificant >>. many parseExpr .>> eof

// Parsing helpers to avoid requiring FParsec dependency in other modules.
type ReplParseError =
    | IncompleteInput
    | ParseError of string

let runReadReplExpr (input: string) : Result<LispVal, ReplParseError> =
    match run parseInteractive input with
    | Success(result, _, _) -> Result.Ok result
    | Failure(msg, err, _) ->
        let errIdx = err.Position.Index
        let lastIdx = input.Length

        if errIdx = lastIdx then
            // Unexpected eof; wait for more input to ensure the expression is
            // complete.
            IncompleteInput
        else
            ParseError msg
        |> Result.Error

let runParseFile (f: string) : Result<LispVal list, string> =
    match runParserOnFile parseManyExprs () f System.Text.Encoding.UTF8 with
    | Success(exprs, _, _) -> Result.Ok exprs
    | Failure(msg, _, _) -> Result.Error msg
