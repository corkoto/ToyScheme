# Partial Scheme toy implementation
A partial implementation of Scheme with initial functionality based on the first half of
[Write Yourself a Scheme in 48 Hours](https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours).
Made for learning purposes; not intended as a serious Scheme implementation in any way.
Several features have been added that are not part of the guide.
Uses mutable state for the evaluation environment (encapsulated using a class) and
[FsToolkit.ErrorHandling](https://github.com/demystifyfp/FsToolkit.ErrorHandling) for
error handling with the Result type.

## Features added
* Heap-based allocation
* Proper lexical scoping with first-class functions using closures under the hood
* Garbage collection (root based)
* let, let*, letrec
* define and set with semantics based on scoping

## Scheme functionality not included
* Standard library aside from basic functionality
* Tail call optimization
* Variation in semantics for some primitives
* Macros
* Quasiqoute
* Probably more

## Usage
Enter a REPL
```
dotnet run --project src/App
```

Evaluate a file (the value of the last expression in the file is used for the result)
```
dotnet run --project src/App <file>
```
